<?php
require_once 'vendor/autoload.php';

use ISL\Manager\PersonManager;


$personManager = new PersonManager();

$personArray = $personManager::create(5);

echo "<table>";
echo "<tr><th>FirstName</th><th>LastName</th><th>Address</th><th>PostCode</th><th>Country</th><th>Company</th></tr>";
foreach ($personArray as $person){        
        echo "<tr>";
	echo "<td>".$person->getFirstName()."</td>";
        echo "<td>".$person->getLastName()."</td>";
        echo "<td>".$person->getAddress()."</td>";
        echo "<td>".$person->getPostCode()."</td>";
        echo "<td>".$person->getCountry()."</td>";
        echo "<td>".$person->getCompany()."</td>";
        echo "</tr>";      
}
echo "</table>";
