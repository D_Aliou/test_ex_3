<?php
namespace ISL\Entity;

class Person {
	private $firstName;
	private $lastName;
	private $address;
	private $postCode;
	private $country;
	private $company;

	public function __construct($firstName, $lastName, $address, $postCode, $country, $company){
		$this->setFirstName($firstName);
		$this->setLastName($lastName);
		$this->setAddress($address);
		$this->setPostCode($postCode);
		$this->setCountry($country);
		$this->setCompany($company);
	}

	public function getFirstName(){
		return $this->firstName;
	}
	public function setFirstName($firstName){
		$this->firstName = $firstName;
	}

	public function getLastName(){
		return $this->lastName;
	}
	public function setLastName($lastName){
		$this->lastName = $lastName;
	}

	public function getAddress(){
		return $this->address;
	}
	public function setAddress($address){
		$this->address = $address;
	}

	public function getPostCode(){
		return $this->postCode;
	}
	public function setPostCode($postCode){
		$this->postCode = $postCode;
	}

	public function getCountry(){
		return $this->country;
	}
	public function setCountry($country){
		$this->country = $country;
	}

	public function getCompany(){
		return $this->company;
	}
	public function setCompany($company){
		$this->company = $company;
	}
}
