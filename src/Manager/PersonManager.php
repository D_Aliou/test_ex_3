<?php
namespace ISL\Manager;

use ISL\Entity\Person;
use Faker\Factory;

class PersonManager {
	public static function create($number){
		//initialisation de faker
		$faker = Factory::create('fr_FR');
		$personArray = [];

		for($i = 0; $i < $number; $i++){
			$personArray[$i] = new Person(
				$faker->firstName,
				$faker->lastName,
				$faker->streetAddress,
				$faker->postcode,
				$faker->country,
				$faker->company
			);
		}

		return $personArray;
	}
}
